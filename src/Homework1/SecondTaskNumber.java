package Homework1;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class SecondTaskNumber {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int[] results = new int[10];
        int[] userGuesses = new int[10]; // Масив для збереження всіх введених чисел
        int userGuessesCounter = 0; // Лічильник для коректного збереження в масив

        System.out.println("Let the game begin!");

        String[][] events = {
                {"1939", "World War II began"},
                {"1969", "First human on the Moon"},
                {"2007", "Introduction of the iPhone"},
                {"1492", "Christopher Columbus sailed to America"}
        };

        for (int i = 0; i < results.length; i++) {
            System.out.println("Enter your name: ");
            String name = scanner.nextLine();

            // Вибір випадкової події
            int eventIndex = random.nextInt(events.length);
            int year = Integer.parseInt(events[eventIndex][0]);
            String event = events[eventIndex][1];

            System.out.println("When did the " + event + "?");

            while (true) {
                System.out.println("Enter your guess (between 0 and 2100): ");
                if (scanner.hasNextInt()) {
                    int enteredNum = scanner.nextInt();
                    scanner.nextLine();

                    // Зберігання введеного числа в масив
                    userGuesses[userGuessesCounter++] = enteredNum;

                    if (enteredNum < 0 || enteredNum > 2100) {
                        System.out.println("Invalid input. Please enter a valid number between 0 and 2100.");
                    } else if (enteredNum != year) {
                        System.out.println("Your guess is incorrect. Please try again.");
                    } else {
                        System.out.println("Congratulations, " + name + "!");
                        results[i] = 1; // 1 представляє вгадану гру
                        // Копіювання та сортування лише тих чисел, які ввів користувач
                        int[] sortedUserGuesses = Arrays.copyOfRange(userGuesses, 0, userGuessesCounter);
                        Arrays.sort(sortedUserGuesses);
                        System.out.println("Your numbers: " + Arrays.toString(sortedUserGuesses));
                        break;
                    }
                } else {
                    System.out.println("Invalid input. Please enter a valid number.");
                    scanner.nextLine();
                }
            }
        }

    }
}