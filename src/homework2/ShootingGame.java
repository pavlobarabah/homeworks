package homework2;
public class ShootingGame {
    public static void main(String[] args) {
        ShootingController shootingController = new ShootingController();
        char[][] gameBoard = shootingController.initializeGameBoard();
        shootingController.displayGameBoard(gameBoard);

        System.out.println("All Set. Get ready to rumble!");

        while (true) {
            int row = shootingController.getValidInput("Enter row (1-5): ");
            int col = shootingController.getValidInput("Enter column (1-5): ");

            if (gameBoard[row][col] == 'X') {
                shootingController.displayGameBoard(gameBoard);
                System.out.println("You have won!");
                break;
            } else {
                System.out.println("Missed! Try again.");
                gameBoard[row][col] = '*';
                shootingController.displayGameBoard(gameBoard);
            }

        }
    }

}

