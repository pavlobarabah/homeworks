package Homework4;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    static {
        System.out.println("Class Human is being loaded.");
    }

    {
        System.out.println("A new Human object is being created.");
    }
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private String[][] schedule;
    private Family family;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human(String name, String surname, int year, int iq, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = new String[7][2];
    }

    public void greetPet(Pet pet) {
        System.out.println("Привіт, " + pet.getNickname());
    }

    public void describePet() {
        String intelligence = (pet.getTrickLevel() > 50) ? "дуже хитрий" : "майже не хитрий";
        System.out.println("У мене є " + pet.getSpecies() + ", їй " + pet.getAge() +
                " років, він " + intelligence + ".");
    }

    public boolean feedPet(boolean isFeedingTime) {
        if (isFeedingTime) {
            System.out.println("Хм... годувати " + pet.getNickname() + ".");
            pet.eat();
            return true;
        } else {
            Random random = new Random();
            int randomTrickLevel = random.nextInt(101);

            if (pet.getTrickLevel() > randomTrickLevel) {
                System.out.println("Думаю, " + pet.getNickname() + " не голодний.");
                return false;
            } else {
                System.out.println("Хм... годувати " + pet.getNickname() + ".");
                pet.eat();
                return true;
            }
        }
    }


    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq=" + iq +
                ", schedule=" + Arrays.deepToString(schedule) +
                ", family=" + (family != null ? family.getFamilyInfoForHuman() : "null") +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year);
    }

    public Family getFamily() {
        return family;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
