package Homework3;

import java.util.Scanner;

public class TaskController {
    Scanner scanner = new Scanner(System.in);
    private String[][] schedule = new String[7][2];
    private boolean isTrue = true;
    private String input;

    public void addSchedule(){
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to university";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "go to university; go English courses";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "go to university; do home work; relax";
        schedule[5][0] = "Friday";
        schedule[5][1] = "go to university; go for a walk with friends";
        schedule[6][0] = "Saturday";
        schedule[6][1] = "do house work; relax";
    }

    public void chooseDay(){
        while (isTrue){
            System.out.print("Please, input the day of the week: ");
            input = scanner.nextLine().trim().toLowerCase();
            switch (input){
                case "monday":
                    System.out.println("Your tasks for " + schedule[1][0] + ": " + schedule[1][1]);
                    break;
                case "sunday":
                    System.out.println("Your tasks for " + schedule[0][0] + ": " + schedule[0][1]);
                    break;
                case "tuesday":
                    System.out.println("Your tasks for " + schedule[2][0] + ": " + schedule[2][1]);
                    break;
                case "wednesday":
                    System.out.println("Your tasks for " + schedule[3][0] + ": " + schedule[3][1]);
                    break;
                case "thursday":
                    System.out.println("Your tasks for " + schedule[4][0] + ": " + schedule[4][1]);
                    break;
                case "friday":
                    System.out.println("Your tasks for " + schedule[5][0] + ": " + schedule[5][1]);
                    break;
                case "saturday":
                    System.out.println("Your tasks for " + schedule[6][0] + ": " + schedule[6][1]);
                    break;
                case "exit":
                    isTrue = false;
                    break;
                case "change":
                case "reschedule":
                    change(input, schedule);
                    break;
                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }

    public void change(String input, String[][] schedule) {
        System.out.print("Please, input the day of the week to change or reschedule tasks: ");
        String dayToChange = scanner.nextLine().trim().toLowerCase();

        int dayIndex = getDayIndex(dayToChange, schedule);
        if (dayIndex != -1) {
            System.out.print("Please, input new tasks for " + schedule[dayIndex][0] + ": ");
            String newTasks = scanner.nextLine();
            schedule[dayIndex][1] = newTasks;
            System.out.println("Tasks for " + schedule[dayIndex][0] + " have been updated.");
        } else {
            System.out.println("Invalid day. Please try again.");
        }
    }
    public int getDayIndex(String day, String[][] schedule) {
        for (int i = 0; i < schedule.length; i++) {
            if (schedule[i][0].equalsIgnoreCase(day)) {
                return i;
            }
        }
        return -1;
    }

}
