package homework2;

import java.util.Random;
import java.util.Scanner;

public class ShootingController {
    public char[][] initializeGameBoard() {
        char[][] board = new char[6][6];
        Random random = new Random();

        int targetRow = random.nextInt(3) + 1;
        int targetCol = random.nextInt(3) + 1;

        for (int i = targetRow; i < targetRow + 3; i++) {
            for (int j = targetCol; j < targetCol + 3; j++) {
                board[i][j] = 'X';
            }
        }
        return board;
    }

    public void displayGameBoard(char[][] board) {
        System.out.println("  | 1 | 2 | 3 | 4 | 5 |");
        System.out.println("-----------------------");
        for (int i = 1; i <= 5; i++) {
            System.out.print(i + " | ");
            for (int j = 1; j <= 5; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
        }
        System.out.println("-----------------------");
    }

    public int getValidInput(String message) {
        Scanner scanner = new Scanner(System.in);
        int input;
        while (true) {
            System.out.print(message);
            input = Integer.parseInt(scanner.nextLine());
            if (input >= 1 && input <= 5) {
                break;
            } else {
                System.out.println("Invalid input. Please enter a number between 1 and 5.");
            }

        }
        return input;
    }
}
