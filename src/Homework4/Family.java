package Homework4;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    static {
        System.out.println("Class Family is being loaded.");
    }

    {
        System.out.println("A new Family object is being created.");
    }
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;

    public Family(Human mother, Human father) {
        if (mother != null && father != null) {
            this.mother = mother;
            this.father = father;
            this.mother.setFamily(this);
            this.father.setFamily(this);
            this.children = new Human[0];
        }
    }

    public void addChildren(Human child) {
        if (child != null) {
            Human[] newChildren = Arrays.copyOf(children, children.length + 1);
            newChildren[children.length] = child;
            this.children = newChildren;
            child.setFamily(this);
        }
    }

    public boolean deleteChild(Human child) {
        if (child == null || children.length == 0) {
            return false;
        }

        int index = -1;
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(child)) {
                index = i;
                break;
            }
        }

        if (index == -1) {
            return false; // Дитина не знайдена в масиві
        }

        Human[] newChildren = new Human[children.length - 1];
        System.arraycopy(children, 0, newChildren, 0, index);
        System.arraycopy(children, index + 1, newChildren, index, children.length - index - 1);

        children = newChildren;
        child.setFamily(null);
        return true;
    }

    public int countFamily(){
        int familyNum = 2;
        if (children.length > 0){
            familyNum += children.length;
        }

        return familyNum;
    }

    public String getFamilyInfoForHuman() {
        return "Family{" +
                "mother=" + (mother != null ? mother.getName() : "null") +
                ", father=" + (father != null ? father.getName() : "null") +
                ", children=" + getChildrenInfo() +
                ", pet=" + (pet != null ? pet.getNickname() : "null") +
                '}';
    }

    private String getChildrenInfo() {
        if (children.length == 0) {
            return "[]";
        }

        StringBuilder childrenInfo = new StringBuilder("[");
        for (Human child : children) {
            childrenInfo.append(child.getName()).append(", ");
        }
        childrenInfo.delete(childrenInfo.length() - 2, childrenInfo.length()); // Remove the last comma and space
        childrenInfo.append("]");
        return childrenInfo.toString();
    }


    @Override
    public String toString() {
        return getFamilyInfoForHuman();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) && Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father);
    }


    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }
}
