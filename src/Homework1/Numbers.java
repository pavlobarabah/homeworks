package Homework1;

import java.util.Random;
import java.util.Scanner;

public class Numbers {
    public static void main(String[] args) {
        Random random = new Random();
        Scanner scanner = new Scanner(System.in);
        int[] results = new int[10];
        String name;

        System.out.println("Let the game begin!");

        for (int i = 0; i < results.length; i++) {
            System.out.println("Enter your name: ");
            name = scanner.nextLine();

            int randomNumber = random.nextInt(100);
//            System.out.println(randomNumber);

            while (true) {
                System.out.println("Enter your guess (between 0 and 100): ");
                int enteredNum = scanner.nextInt();
                scanner.nextLine();

                if (enteredNum < randomNumber) {
                    System.out.println("Your number is too small. Please, try again.");
                } else if (enteredNum > randomNumber) {
                    System.out.println("Your number is too big. Please, try again.");
                } else {
                    System.out.println("Congratulations, " + name + "!");
                    break;
                }
            }
        }
    }
}


