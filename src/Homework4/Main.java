package Homework4;

public class Main {
    public static void main(String[] args) {

        Pet cat = new Pet("Cat", "Whiskers", 3, 60, new String[]{"Scratching furniture", "Playing with a ball"});
        Pet dog = new Pet("Dog", "Buddy", 5, 70, new String[]{"Fetching the ball", "Barking"});

        Human mom = new Human("Anna", "Koval", 1980, 120, cat);
        Human dad = new Human("Borys", "Koval", 1982, 130, dog);
        Human child = new Human("Maria", "Koval", 2005);

        Family family = new Family(mom, dad);
        family.addChildren(child);
        family.setPet(cat);

        child.setFamily(family);

        System.out.println("---------------------------------------------------------------------------------------------------");
        mom.greetPet(cat);
        dad.describePet();
        child.greetPet(cat);

        System.out.println("Family Information:");
        System.out.println(family);

        System.out.println("---------------------------------------------------------------------------------------------------");

        System.out.println("Mom's Information:");
        System.out.println(mom);

        System.out.println("---------------------------------------------------------------------------------------------------");

        System.out.println("Dad's Information:");
        System.out.println(dad);

        System.out.println("---------------------------------------------------------------------------------------------------");

        System.out.println("Child's Information:");
        System.out.println(child);

        System.out.println("---------------------------------------------------------------------------------------------------");

        System.out.println("Pet's Information:");
        System.out.println(cat);

        System.out.println("---------------------------------------------------------------------------------------------------");

        family.deleteChild(child);
        System.out.println("Family Information After Deleting a Child:");
        System.out.println(family);

        System.out.println("---------------------------------------------------------------------------------------------------");

        System.out.println("Number of Family Members: " + family.countFamily());

        System.out.println("---------------------------------------------------------------------------------------------------");

        Human newChild = new Human("Lilia", "Koval", 2010);
        family.addChildren(newChild);
        System.out.println("Family Information After Adding a New Child:");
        System.out.println(family);
        System.out.println("Number of Family Members: " + family.countFamily());

        System.out.println("---------------------------------------------------------------------------------------------------");

        dog.respond();
        dog.foul();

        System.out.println("---------------------------------------------------------------------------------------------------");

        cat.respond();
        cat.foul();

        System.out.println("---------------------------------------------------------------------------------------------------");

        dad.feedPet(true);
        dad.feedPet(false);

        System.out.println("---------------------------------------------------------------------------------------------------");

        mom.feedPet(true);
        mom.feedPet(false);

        System.out.println("---------------------------------------------------------------------------------------------------");


    }
}

